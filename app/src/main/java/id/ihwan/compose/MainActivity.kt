package id.ihwan.compose

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.compose.Model
import androidx.ui.core.CurrentTextStyleProvider
import androidx.ui.core.Text
import androidx.ui.core.dp
import androidx.ui.core.setContent
import androidx.ui.graphics.Color
import androidx.ui.layout.Column
import androidx.ui.layout.CrossAxisAlignment
import androidx.ui.layout.ExpandedHeight
import androidx.ui.layout.Spacing
import androidx.ui.material.*
import androidx.ui.material.surface.Surface
import androidx.ui.text.TextStyle
import androidx.ui.tooling.preview.Preview

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CustomTheme{
                MyScreenContent()
            }
        }
    }
}

val green = Color(0xFF1EB980.toInt())
val grey = Color(0xFF26282F.toInt())

private val themeColors = MaterialColors(
        primary = green,
        surface = grey,
        onSurface = Color.White
)

@Composable
fun CustomTheme(children: @Composable() () -> Unit) {
    MaterialTheme(colors = themeColors) {
        val textStyle = TextStyle(color = Color.Red)
        CurrentTextStyleProvider(value = textStyle) {
            children()
        }
    }
}

@Composable
fun MyApp(children: @Composable() () -> Unit) {
    MaterialTheme {
        Surface(color = Color.Blue) {
            children()
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text (text = "Hello $name!", modifier = Spacing(24.dp))
}

@Composable
fun MyScreenContent(names: List<String> = listOf("Android", "iOS", "Flutter", "Nokia"),
                    counterState: CounterState = CounterState(),
                    formState: FormState = FormState(true)) {
    Column(modifier = ExpandedHeight, crossAxisAlignment = CrossAxisAlignment.Center) {
        Column(Flexible(1f), crossAxisAlignment = CrossAxisAlignment.Center) {
            for (name in names) {
                Greeting(name = name)
                Divider(color = Color.Black)
            }
        }
        Divider(color = Color.Transparent, height = 32.dp)
        Counter(counterState)
        Form(formState = formState)
    }
}

@Composable
fun Counter(state: CounterState) {
    Button(
            text = "I've been clicked ${state.count} times",
            onClick = {
                state.count++
            },
            style = ContainedButtonStyle(color = if (state.count > 5) Color.Green else Color.White)
    )
}

@Composable
fun Form(formState: FormState) {
    Checkbox(
            checked = formState.optionChecked,
            onCheckedChange = { newState -> formState.optionChecked = newState })
}

@Preview
@Composable
fun DefaultPreview() {
    CustomTheme{
       MyScreenContent()
   }
}

@Model
class FormState(var optionChecked: Boolean)

@Model
class CounterState(var count: Int = 0)
